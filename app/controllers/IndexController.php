<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $this->view->testme = 'thi is a test';

        // Passing more than one variable at the same time
        // $this->view->setVars(
        //     [
        //         'username' => $user->username,
        //         'posts' => $posts,
        //     ]
        // );

        $this->view->pick('public/one');
    }
}
