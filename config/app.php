<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;

// Create a DI
$di = new FactoryDefault();

// Setup the view component
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir(APP_PATH . '/views/');
        return $view;
    }
);

// Setup a base URI
$di->set(
    'url',
    function () {
        $url = new UrlProvider();
        $url->setBaseUri('/');
        return $url;
    }
);

// set up the app
$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (Exception $e) {
    $logger->critical(
        $e->getMessage()
    );
    echo 'Exception: ', $e->getMessage();
}