<?php

use Phalcon\Logger\Adapter\File as FileAdapter;

define('LOG_PATH', APP_PATH . '/logs');

try {
    $logger = new FileAdapter(LOG_PATH . '/phalcon.log');
} catch (Exception $e) {
    echo 'Couldnt load the File adapter, ' . $e->getMessage();
}