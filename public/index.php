<?php

use Phalcon\Logger;

// Define some absolute path constants to aid in locating resources
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
define('CONFIG_PATH', BASE_PATH . '/config');

/**
 * Include composer autoloader
 */
require BASE_PATH   . "/vendor/autoload.php";
require CONFIG_PATH . '/log.php';
require CONFIG_PATH . '/autoload.php';
require CONFIG_PATH . '/app.php';
require CONFIG_PATH . '/db.php';

// check if some helpers exist
if ( ! function_exists('dd') ) {
    throw new Exception('dd() function doesnt exist');
}

$loader->registerNamespaces(
    [
        'App\Models' => '../app/models/',
        'App\Controllers' => '../app/controllers/',
    ]
);